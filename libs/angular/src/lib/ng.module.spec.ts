import { TestBed } from '@angular/core/testing';

import { ReactiveModule } from './ng.module';

describe(`ReactiveModule`, () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveModule]
    });
  });

  it('should be created', () => {});
});
