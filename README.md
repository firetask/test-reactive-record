# Reactive Record [![Build Status](https://travis-ci.org/Firetask/reactive-record.svg?branch=master)](https://travis-ci.org/Firetask/reactive-record) [![codecov](https://codecov.io/gh/Firetask/reactive-record/branch/master/graph/badge.svg)](https://codecov.io/gh/Firetask/reactive-record) [![Conventional Commits](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](https://conventionalcommits.org)

> Attention
>
> This repository is destinated to run the CI/CD stuff. For issues and pull requests please use the main repo [`Firestask`](https://github.com/firetask/firetask)

## Docs

Coming soon

## License

MIT
